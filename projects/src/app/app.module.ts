import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { registerLocaleData } from '@angular/common';
import localeVi from '@angular/common/locales/vi';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { VLayoutModule } from '@vnpt/oneui-ui/layout';
import { VGridModule } from '@vnpt/oneui-ui/grid';
import { VButtonModule } from '@vnpt/oneui-ui/button';
import { VIconModule } from '@vnpt/oneui-ui/icon';
import { VTypographyModule } from '@vnpt/oneui-ui/typography';
import { VDropDownModule } from '@vnpt/oneui-ui/dropdown';
import { VBreadCrumbModule } from '@vnpt/oneui-ui/breadcrumb';
import { VLogoModule } from '@vnpt/oneui-ui/logo';
import { VSelectModule } from '@vnpt/oneui-ui/select';
import { VCardModule } from '@vnpt/oneui-ui/card';
import { VInputModule } from '@vnpt/oneui-ui/input';
import { VDrawerModule } from '@vnpt/oneui-ui/drawer';
import { VAvatarModule } from '@vnpt/oneui-ui/avatar';
import { VBadgeModule } from '@vnpt/oneui-ui/badge';
import { VTableModule } from '@vnpt/oneui-ui/table';
import { VAffixModule } from '@vnpt/oneui-ui/affix';
import { MainComponent } from './main/main.component';
import { VListModule } from '@vnpt/oneui-ui/list';
import { VNotificationModule } from '@vnpt/oneui-ui/notification';
import { VMessageModule } from '@vnpt/oneui-ui/message';

registerLocaleData(localeVi);

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    VLayoutModule,
    VGridModule,
    VButtonModule,
    VIconModule,
    VTypographyModule,
    VBreadCrumbModule,
    VDropDownModule,
    VLogoModule,
    VInputModule,
    VDrawerModule,
    VAvatarModule,
    VBadgeModule,
    VAffixModule,
    HttpClientModule,
    VListModule,
    VNotificationModule,
    VMessageModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
