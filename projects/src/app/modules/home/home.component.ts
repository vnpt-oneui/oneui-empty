import { AfterViewInit,
  Component,
  ComponentFactoryResolver,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import { Subscription } from 'rxjs';

@Component({
  selector: 'mainapp-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  constructor() {}

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {

  }
}
